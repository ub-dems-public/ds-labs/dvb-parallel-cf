
sub module {
	local ($exec_prefix);

	$exec_prefix = "/usr/bin";
	eval `$exec_prefix/modulecmd perl @_`;
}

$ENV{"MODULESHOME"} = "/usr/share/Modules";

if (! defined $ENV{"MODULEPATH"} ) {
	open(IN, "$ENV{'MODULESHOME'}/init/.modulespath") || die "cannot open '.modulespath' file: $!\n";
	$ENV{"MODULEPATH"} = join(":", grep(/\S/, map { s/^\s*//g; s/[\s#].*$//g; $_ } <IN>));
	close IN;
}

if (! defined $ENV{"LOADEDMODULES"} ) {
	$ENV{"LOADEDMODULES"} = "";
}

1;
