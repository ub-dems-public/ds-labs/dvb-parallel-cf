import os, re, subprocess


if not os.environ.has_key('MODULEPATH'):
	f = open(os.environ['MODULESHOME'] + "/init/.modulespath", "r")
	path = []
	for line in f.readlines():
		line = re.sub("#.*$", '', line)
		if line is not '':
			path.append(line)
	os.environ['MODULEPATH'] = ':'.join(path)

if not os.environ.has_key('LOADEDMODULES'):
	os.environ['LOADEDMODULES'] = ''
	
def module(*args):
	if type(args[0]) == type([]):
		args = args[0]
	else:
		args = list(args)
	(output, error) = subprocess.Popen(['/usr/bin/modulecmd', 'python'] + 
			args, stdout=subprocess.PIPE).communicate()
	exec output

