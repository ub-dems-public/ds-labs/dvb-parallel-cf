Coursera: "Fundamentals of Parallelism on Intel Architecture"
=============================================================

by Andrey Vladimirov (Intel)

* https://www.coursera.org/learn/parallelism-ia


```

# run with:

make && make queue && watch -n 2 bash -c "qstat; ls -l n*.o*"; bash -c "cat n*.o*"


# stats with:


find . -name '*.out.txt' -exec grep -H '.' {} \; | perl -pane 's{solutions/([^/]+).*\.out\.txt}{$1}' | less -SRX






```

